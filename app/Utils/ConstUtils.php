<?php
/**
 * Created by PhpStorm.
 * User: valeria.castelo
 * Date: 20/01/2017
 * Time: 11:00
 */

namespace App\Utils;

class ConstUtils {


//Destino dos arquivos e imagens salvos
const CAMINHO_ARQUIVO_RG = '/docs/rg';
const CAMINHO_ARQUIVO_CPF = '/docs/cpf';
const CAMINHO_ARQUIVO_TITULO = '/docs/titulo';
const CAMINHO_ARQUIVO_QUITACAO = '/docs/quitacao';
const CAMINHO_ARQUIVO_PIS = '/docs/pis';
const CAMINHO_ARQUIVO_RESERVISTA = '/docs/reservista';
const CAMINHO_ARQUIVO_RESIDENCIA = '/docs/residencia';
const CAMINHO_ARQUIVO_CERTIDAO = '/docs/certidao';
const CAMINHO_ARQUIVO_ESCOLARIDADE = '/docs/escolaridade';
const CAMINHO_ARQUIVO_CURSO = '/docs/curso';
const CAMINHO_ARQUIVO_REGISTRO = '/docs/registro';
const CAMINHO_ARQUIVO_BANCO = '/docs/banco';
const CAMINHO_ARQUIVO_EXPERIENCIA_1 = '/docs/experiencia_1';
const CAMINHO_ARQUIVO_EXPERIENCIA_2 = '/docs/experiencia_2';
const CAMINHO_ARQUIVO_EXPERIENCIA_3 = '/docs/experiencia_3';
const CAMINHO_ARQUIVO_RECURSO = '/docs/recursos';

//extensão de arquivo PDF
const EXTENSION_DOC = 'PDF';

//tamanho máximo dos arquivos
const TAMANHO_ARQUIVO = '5000000';


}