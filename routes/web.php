<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::resource('/profile',ProfileController::class);

Route::get('/', 'ProfileController@inicio');
// Route::get('/profile', 'ProfileController@index');
// Route::put('/profile', 'ProfileController@store');
// // Route::get('/profile', 'ProfileController@create');
// Route::get('/candidato/criar', 'CandidatoController@create');
// // Route::store('/candidato/criar', 'CandidatoController@store');

/*** Formulário processo seletivo candidato***/
Route::resource('processo-candidato', 'ProcessoCandidatoController');
/*Route::post('/processo-candidato/criar', 'ProcessoCandidatoController@create');*/
/*Route::post('/processo-candidato/criar', 'ProcessoCandidatoController@exibirTelaProcesso')->name('tela.processo');;*/
